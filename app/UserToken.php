<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserToken
 * @property int $id
 * @property int $user_id
 * @property string $token
 */
class UserToken extends Model
{
    protected $table = 'users_tokens';

    protected $fillable = [
        'user_id',
        'token'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @param $token
     * @return null
     */
    public static function getUserIdByToken($token) {
        $user_token = static::where('token', $token)
            ->first();

        $auth_user_id = null;
        if($user_token) {
            $auth_user_id = $user_token->user_id;
        }

        return $auth_user_id;
    }
}
