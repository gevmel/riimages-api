<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ImageComment
 * @property int $id
 * @property int $image_id
 * @property int $creator_id
 * @property string $content
 */

class ImageComment extends Model
{
    protected $table = 'image_comments';

    protected $fillable = [
        'image_id',
        'creator_id',
        'content'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
