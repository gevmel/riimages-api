<?php
/**
 * Created by PhpStorm.
 * User: gevorg
 * Date: 6/23/18
 * Time: 3:41 AM
 */

namespace App\Helpers;


use App\User;
use App\UserToken;

class AuthHelper
{
    public static function isPermitted($user_id, $token) {
        $user = User::findOrFail($user_id);

        $token_user = UserToken::where('token', $token)
            ->first();

        return $user->id == $token_user->user_id;
    }
}