<?php
/**
 * Created by PhpStorm.
 * User: gevorg
 * Date: 6/22/18
 * Time: 4:53 AM
 */

namespace App\Helpers;

/**
 * Class UnsplashAPI
 * @property string $keyword
 * @property string $base_url
 * @property string $client_id
 */

class UnsplashAPI
{

    private $keyword;
    private $base_url;
    private $client_id;

    /**
     * @param string $keyword
     * @param int $rpp
     * @return void
     */
    public function __construct($keyword = '', $rpp = 10) {
        $this->base_url = env('UNSPLASH_API_BASE_URL');
        $this->client_id = env('UNSPLASH_API_KEY');
        $this->keyword = $keyword;
    }

    /**
     * @param int page
     * @return array
     */
    public function get_images_array_for_page($page = 1) {
        $url = $this->base_url . '?query=' . $this->keyword . '&page=' . $page . '&client_id=' . $this->client_id;

        $json_output = CurlHelper::request_to_url($url);

        $result = [];

        if (count($json_output)) {
            $images_array = $json_output->results;

            if ($images_array) {
                foreach ($images_array as $image_data) {
                    $result_image = [];
                    $result_image['url'] = $image_data->urls->full;
                    $result_image['description'] = $image_data->description;
                    $result_image['title'] = $image_data->user->username;

                    array_push($result, $result_image);
                }
            }

        }

        return $result;
    }

}