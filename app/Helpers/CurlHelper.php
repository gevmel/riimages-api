<?php
/**
 * Created by PhpStorm.
 * User: gevorg
 * Date: 6/22/18
 * Time: 4:43 AM
 */

namespace App\Helpers;


class CurlHelper
{

    public static function request_to_url($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
        curl_close($ch);

        $json_output = json_decode($output);

        return $json_output;

    }

    /**
     * @param $url
     * @param array $headers
     * @return array
     */
    public static function download_image($url, $headers = ['Accept: application/json']) {

        $image_name = 'image-' . time() . '.jpg';

        $full_path = storage_path('app/public/') . $image_name;

        $fp = fopen($full_path, 'w');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 500,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_VERBOSE => 1,
            CURLOPT_FILE => $fp
        ));

        curl_exec($curl);
        $err = curl_error($curl);
        $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        fclose($fp);

        if ($err) {
            $result = [
                'error' => 'cURL Error #:' . $err
            ];
        } else {
            if ($response_code != 200) {
                unlink($full_path);
                return null;
            }
            $result = [
                'image_path' => $full_path,
                'image_url' => url('/storage') . '/' . $image_name
            ];
        }

        return $result;

    }

}