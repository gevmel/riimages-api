<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelper;
use App\ImageComment;
use App\Images;
use App\User;
use Illuminate\Http\Request;

class ImageCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $image_id)
    {
        $image = Images::find($image_id);

        if (!$image) {
            return response()->json(['message' => 'The image does not exist.'], 404);
        }

        $rpp = env('RPP') ?? 10;
        $page = $request->input('page') ?? 1;

        $paginated_query = ImageComment::where('image_id', $image->id)
            ->paginate($rpp, ['*'], null, $page);

        foreach ($paginated_query as $item) {
            $item->creator = User::findOrFail($item->creator_id);
        }

        return response()->json(['comments' => $paginated_query], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $image_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(Request $request, $image_id)
    {
        $image = Images::findOrFail($image_id);
        $user = User::findOrFail($request->input('creator_id'));

        if (!AuthHelper::isPermitted($user->id, $request->input('auth_token'))) {
            return response()->json(['message' => 'Forbidden'], 403);
        }

        $content = $request->input('content');

        if (empty($content)) {
            return response()->json(['message' => 'Unable to create an empty comment'], 400);
        }

        $comment = new ImageComment();

        $comment->image_id = $image->id;
        $comment->creator_id = $user->id;
        $comment->content = $content;

        $comment->saveOrFail();

        return response()->json(['comment' => $comment], 200);
    }
}
