<?php

namespace App\Http\Controllers;

use App\Images;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->input('page') ?? 1;
        $rpp = env('RPP');

        $paginated_query = Images::query()->paginate($rpp, ['*'], null, $page);

        foreach ($paginated_query as $item) {
            $item->total = $paginated_query->total();
        }

        return response()->json($paginated_query, 200);
    }
}
