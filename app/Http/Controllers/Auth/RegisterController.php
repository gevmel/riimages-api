<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserToken;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mockery\Exception;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([$errors], 400);
        }

        $username = $request->input('username');

        $user = User::where('username', $username)
            ->first();

        if ($user) {
            return response()->json(['message' => 'The user already exists.'], 403);
        }

        try {
            DB::beginTransaction();

            $user = new User();
            $user->username = $username;
            $user->save();

            $rnd_token_str = str_random(60);

            $user_token = new UserToken();
            $user_token->user_id = $user->id;
            $user_token->token = $rnd_token_str;

            $user_token->save();

            DB::commit();

            $user->token = $user_token->token;

            return response()->json(['user' => $user], 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            DB::rollback();
            return response()->json(['message' => 'Unable to create a user'], 500);
        }
    }
}
