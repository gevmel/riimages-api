<?php

namespace App\Http\Middleware;

use App\UserToken;
use Closure;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth_token = $request->input('auth_token');
        $result = [];

        if (!$auth_token) {
            $result['message'] = 'Auth token not sent';
            return response()->json($result, 400);
        }

        $user_id = UserToken::getUserIdByToken($auth_token);
        \Auth::onceUsingId($user_id);

        if ($user_id) {
            return $next($request);
        }

        $result['message'] = 'Invalid auth token sent';

        return response()->json($result, 500);
    }
}
