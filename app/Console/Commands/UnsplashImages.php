<?php

namespace App\Console\Commands;

use App\Helpers\CurlHelper;
use App\Helpers\UnsplashAPI;
use App\Images;
use Illuminate\Console\Command;

class UnsplashImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get images from Unsplash API.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     * @return mixed
     */
    public function handle()
    {
        $keyword = 'office';
        $unsplash = new UnsplashAPI($keyword);

        for ($i = 1; $i <= 3; ++$i) {
            $images_array = $unsplash->get_images_array_for_page($i);

            foreach ($images_array as $image_data) {
                $image_links = CurlHelper::download_image($image_data['url']);

                if (!key_exists('error', $image_links)) {

                    $img = Images::where('url', $image_data['url'])
                        ->first();

                    if (!$img) {
                        $img = new Images();
                    }

                    $img->url = $image_links['image_url'];
                    $img->original_url = $image_data['url'];
                    $img->title = $image_data['title'];
                    $img->description = $image_data['description'];

                    $img->saveOrFail();
                }
            }
        }

        return;

    }
}
