<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Images
 *
 * @property int $id
 * @property string $url
 * @property string $original_url
 * @property string $title
 * @property string $description
 */

class Images extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'url',
        'original_url',
        'title',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

}
