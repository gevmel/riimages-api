<?php

use Illuminate\Database\Seeder;

class UsersTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_tokens')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'token' => 'aaabbbccc'
            ],
            [
                'id' => 2,
                'user_id' => 2,
                'token' => '111222333'
            ]
        ]);
    }
}
