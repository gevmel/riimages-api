Installation:
1. under your server root directory run `git clone https://gitlab.com/gevmel/riimages-api.git`
2. `cp .env.example .env` (copy .env.example file into .env)
3. `composer install`
4. `php artisan key:generate`
5. in .env file add your database credentials (DB_USERNAME, DB_PASSWORD)
6. go to your phpmyadmin and create a new database named `riimages-api`
7. `php artisan migrate`
8. `php artisan db:seed`
9. `php artisan storage:link`
10. make sure you have curl installed for your current php version and then run `php artisan get:images`
11. add the virtual host into your .vhosts file referencing to public folder of the project with `ServerName riimages.loc`
12. add `127.0.0.1 riimages.loc` into your hosts file 
13. `sudo chmod -R 777 storage/`
                   