<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::post('auth/register', 'Auth\\RegisterController@register');
Route::post('auth/login', 'Auth\\LoginController@login');

// Get all images with pagination
Route::get('images', 'ImagesController@index')->middleware('token-auth');

// Image Comments
Route::group(['prefix' => 'images/{image_id}', 'middleware' => 'token-auth'], function () {
    Route::resource('comments','ImageCommentsController');
});
